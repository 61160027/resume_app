import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget profileSection = Container(
      decoration: BoxDecoration(color: Colors.blue[900]),
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Container(
            child: Image.asset('images/profile.jpg',
                width: 120, height: 120, fit: BoxFit.cover),
          ),
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.only(bottom: 8, left: 20),
                  child: Text(
                    'Computer science',
                    style: TextStyle(color: Colors.white),
                  )),
              Container(
                  padding: const EdgeInsets.only(bottom: 8, left: 20),
                  child: Text(
                    'THANAN',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20),
                  )),
              Container(
                  padding: const EdgeInsets.only(bottom: 8, left: 20),
                  child: Text(
                    'RENGKHUANKHWAI',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.white, fontSize: 20),
                  )),
              Container(
                  padding: const EdgeInsets.only(bottom: 8, left: 20),
                  child: Text(
                    'Date of Birth: 15/11/1999',
                    style: TextStyle(color: Colors.grey[400]),
                  )),
              Container(
                  padding: const EdgeInsets.only(bottom: 8, left: 20),
                  child: Text(
                    'Email: dogford01@gmail.com',
                    style: TextStyle(color: Colors.grey[400]),
                  )),
            ],
          ))
        ],
      ),
    );
    Widget contactSection = Container(
      margin: EdgeInsets.only(top: 20, left: 15, right: 15),
      decoration: const BoxDecoration(
          border: Border(top: BorderSide(width: 2.0, color: Colors.black))),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.only(top: 10, bottom: 8, left: 20),
                  child: Text(
                    'GET IN CONTACT',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  )),
              Container(
                  padding: const EdgeInsets.only(bottom: 8, left: 20),
                  child: Text(
                    'Mobile: 061-2959722',
                    style: TextStyle(color: Colors.grey),
                  )),
              Container(
                  padding: const EdgeInsets.only(bottom: 8, left: 20),
                  child: Text(
                    'Facebook: FordThanan',
                    style: TextStyle(color: Colors.grey),
                  )),
              Container(
                  padding: const EdgeInsets.only(bottom: 8, left: 20),
                  child: Text(
                    'Line: Ford_ky',
                    style: TextStyle(color: Colors.grey),
                  )),
            ],
          ))
        ],
      ),
    );
    Widget educationSection = Container(
      margin: EdgeInsets.only(top: 20, left: 15, right: 15),
      decoration: const BoxDecoration(
          border: Border(top: BorderSide(width: 2.0, color: Colors.black))),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.only(top: 10, bottom: 8, left: 20),
                  child: Text(
                    'EDUCATION HISTORY',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  )),
              Container(
                  padding: const EdgeInsets.only(bottom: 8, left: 20),
                  child: Text(
                    '2012 - 2017: Tarttrakarnkhun School',
                    style: TextStyle(color: Colors.grey),
                  )),
              Container(
                  padding: const EdgeInsets.only(bottom: 8, left: 20),
                  child: Text(
                    '2018: Burapha university',
                    style: TextStyle(color: Colors.grey),
                  )),
            ],
          ))
        ],
      ),
    );
    Widget skillSection = Container(
      margin: EdgeInsets.only(top: 20, left: 15, right: 15),
      decoration: const BoxDecoration(
          border: Border(top: BorderSide(width: 2.0, color: Colors.black))),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        children: [
          Expanded(
              child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                  padding: const EdgeInsets.only(top: 10, bottom: 8, left: 20),
                  child: Text(
                    'SKILL',
                    style: TextStyle(
                        fontWeight: FontWeight.bold, color: Colors.black),
                  )),
              Row(
                children: [
                  Container(
                    child: Image.asset('images/java.png',
                        width: 110, height: 100, fit: BoxFit.cover),
                  ),
                  Container(
                    child: Image.asset('images/html.png',
                        width: 110, height: 120, fit: BoxFit.cover),
                  ),
                  Container(
                    child: Image.asset('images/vue.jpeg',
                        width: 110, height: 100, fit: BoxFit.cover),
                  ),
                  Container(
                    child: Image.asset('images/swift.png',
                        width: 110, height: 100, fit: BoxFit.cover),
                  ),
                ],
              )
            ],
          )),
        ],
      ),
    );
    return MaterialApp(
      title: 'My Resume',
      home: Scaffold(
        appBar: AppBar(
          title: const Text('My Resume'),
        ),
        body: Column(
          children: [
            profileSection,
            contactSection,
            educationSection,
            skillSection
          ],
        ),
      ),
    );
  }
}
